# With no layout
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false
page "/feed.xml", layout: false

Time.zone = "Warsaw"

activate :blog do |blog|
  # blog.permalink = "{year}/{month}/{day}/{title}.html"
  # Matcher for blog source files
  # blog.sources = "{year}-{month}-{day}-{title}.html"
  # blog.taglink = "tags/{tag}.html"
  # blog.summary_length = 250
  # blog.year_link = "{year}.html"
  # blog.month_link = "{year}/{month}.html"
  # blog.day_link = "{year}/{month}/{day}.html"
  blog.layout = "article"
  blog.default_extension = ".md"
  blog.summary_separator = /MORE…/

  blog.tag_template = "tag.html"
  blog.calendar_template = "calendar.html"

  # Enable pagination
  blog.paginate = true
  blog.per_page = 10
  blog.page_link = "page/{num}"
end

activate :directory_indexes
activate :syntax

set :markdown_engine, :redcarpet
set :markdown,
    :fenced_code_blocks => true,
    :smartypants => true,
    :tables => true,
    :autolink => true,
    :footnotes => true

configure :development do
    activate :livereload
end

configure :build do
  activate :minify_css
  activate :minify_javascript
  set :http_prefix, "/blog"
  set :build_dir, 'public'
end
